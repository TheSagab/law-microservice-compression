package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/TheSagab/law-microservice-compression/compress"
)

func compressUploadedFile(w http.ResponseWriter, r *http.Request) {
	ok := auth(r)
	if !ok {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	fmt.Println("File Upload Endpoint Hit")

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	// FormFile returns the first file for the given key `myFile`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, handler, err := r.FormFile("file")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()

	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	// Compress the file with the name result.zip
	zippedFile, err := compress.ZipFiles(file, handler.Filename)
	if err != nil {
		return
	}
	defer zippedFile.Close()
	defer os.Remove("result.zip")
	fi, err := zippedFile.Stat()
	if err != nil {
		// handle error
		return
	}
	w.Header().Set("Content-Disposition", "attachment; filename="+"result.zip")
	http.ServeContent(w, r, "result.zip", fi.ModTime(), zippedFile)
}

func auth(r *http.Request) bool {
	auth := r.Header.Get("Authorization")
	client := &http.Client{}
	req, err := http.NewRequest("GET", "http://oauth.infralabs.cs.ui.ac.id/oauth/resource", nil)
	if err != nil {
		return false
	}

	req.Header.Set("Authorization", auth)
	resp, err := client.Do(req)
	if err != nil {
		return false
	}

	if resp.StatusCode == 200 {
		return true
	}
	return false
}

func main() {
	log.Println("Server is listening on port 20102")
	http.HandleFunc("/upload", compressUploadedFile)
	http.ListenAndServe(":20102", nil)
}
