package compress

import (
	"archive/zip"
	"io"
	"mime/multipart"
	"os"
)

// ZipFiles zips a file with the name result.zip
func ZipFiles(formFile multipart.File, formFileName string) (*os.File, error) {
	newZipFile, err := os.Create("result.zip")
	if err != nil {
		return nil, err
	}

	zipWriter := zip.NewWriter(newZipFile)
	defer zipWriter.Close()

	fileToZip, err := os.Create(formFileName)
	_, err = io.Copy(fileToZip, formFile)
	if err != nil {
		return nil, err
	}
	defer os.Remove(formFileName)
	fileToZip.Seek(0, io.SeekStart)
	// Add files to zip
	err = addFileToZip(zipWriter, fileToZip)
	if err != nil {
		return nil, err
	}
	return newZipFile, err
}

func addFileToZip(zipWriter *zip.Writer, fileToZip *os.File) error {
	// Get the file information
	info, err := fileToZip.Stat()
	if err != nil {
		return err
	}

	header, err := zip.FileInfoHeader(info)
	if err != nil {
		return err
	}

	// Using FileInfoHeader() above only uses the basename of the file. If we want
	// to preserve the folder structure we can overwrite this with the full path.
	header.Name = info.Name()

	// Change to deflate to gain better compression
	// see http://golang.org/pkg/archive/zip/#pkg-constants
	header.Method = zip.Deflate

	writer, err := zipWriter.CreateHeader(header)
	if err != nil {
		return err
	}
	_, err = io.Copy(writer, fileToZip)
	return err
}
